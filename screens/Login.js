import { View, Text, TextInput, TouchableOpacity, Alert } from 'react-native'
import React, { useState } from 'react'

import { storeData } from '../Util';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Login({ navigation }) {
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');

  const login = async () => {
    try {
        const response = await fetch(`http://192.168.100.238:8080/login`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: username,
                password: password
            })
        })

        if (response.ok) {
            const data = await response.json();
            storeData("token", data.token);

            navigation.navigate("Home");
        } else {
            throw new Error("Login bermasalah");
        }
        
    } catch(err) {
        console.log(err);
        Alert.alert("Error login");
    }
  }
    
  return (
    <SafeAreaView>
      <TextInput onChangeText={setUsername} value={username} placeholder="username"/>
      <TextInput onChangeText={setPassword} value={password} placeholder="password"/>
      <TouchableOpacity onPress={login}>
        <Text>Login</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}