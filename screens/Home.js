import { View, Text, BackHandler, TouchableOpacity } from 'react-native'
import React, { useEffect } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'

import { getStoreData, removeDataAsyncStorage } from '../Util';

export default function Home({ navigation }) {

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", () => {
        return true;
    })

    // contoh pengambilan token di async storage
    // fetch("http", {
    //     headers: {
    //         Authorization: getStoreData("token")
    //     }
    // })
  }, []);

  const logout = () => {
    navigation.navigate("Login");
    removeDataAsyncStorage("token");
  }

  return (
    <SafeAreaView>
      <Text>Home</Text>

      <TouchableOpacity onPress={logout}>
        <Text>Logout</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}